package org.frm.spring.firstsprg.conf;

import org.frm.spring.firstsprg.tools.Calculette;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FacadeBean {

    @Bean
    public Calculette getCalculette() {
        return new Calculette();
    }
}
