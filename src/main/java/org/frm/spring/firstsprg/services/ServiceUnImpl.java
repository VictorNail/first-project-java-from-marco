package org.frm.spring.firstsprg.services;

import org.frm.spring.firstsprg.tools.Calculette;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ServiceUnImpl implements IServiceUn {

    private static final Logger log = LoggerFactory.getLogger(ServiceUnImpl.class);

    private Calculette calculette;

    public ServiceUnImpl(Calculette calculette) {
        this.calculette = calculette;
        log.info("ServiceUnImpl created");
    }

    @Override
    public int plus(int a, int b) {
        calculette.plus(a);
        return calculette.getTotal();
    }
}
