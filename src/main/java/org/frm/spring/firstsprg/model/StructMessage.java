package org.frm.spring.firstsprg.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class StructMessage {
    @Id
    private int id;

    private String auteur;
    private Date dateCreation;
    private String content;

}
